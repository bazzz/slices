package slices

import "strings"

// ContainsString reports whether slice c contains a string with same value and case as v.
func ContainsString(c []string, v string) bool {
	for _, x := range c {
		if x == v {
			return true
		}
	}
	return false
}

// ContainsInt reports whether slice c contains an integer with same value as v.
func ContainsInt(c []int, v int) bool {
	for _, x := range c {
		if x == v {
			return true
		}
	}
	return false
}

// ContainsFloat reports whether slice c contains a float64 with same value as v.
func ContainsFloat(c []float64, v float64) bool {
	for _, x := range c {
		if x == v {
			return true
		}
	}
	return false
}

// ContainsUnicodeCaseFoldString reports whether slice c contains a string which is equal to v under Unicode case-folding.
func ContainsUnicodeCaseFoldString(c []string, v string) bool {
	for _, x := range c {
		if strings.EqualFold(x, v) {
			return true
		}
	}
	return false
}

// IndexOfString returns the index of the first element in c with the same value and case as v. It returns -1 if no index was found.
func IndexOfString(c []string, v string) int {
	for i, x := range c {
		if x == v {
			return i
		}
	}
	return -1
}
